/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugaspsc2;

/**
 *
 * @author JONATHAN
 */
import java.util.Scanner;
import  java.awt.geom.Point2D;
import  java.util.LinkedList;
public class TugasPSC2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("masukan panjang dan lebar board board");
        int col = sc.nextInt() , row = sc.nextInt();
        float time = 0;
        Grid [][] board = new Grid [col][row];
        LinkedList<Grid> path = new LinkedList<>();
        System.out.println("masukan isi board 1 untuk rock");
        for(int i  = 0 ; i<board.length; i++){
            for(int j  =0 ; j<board[i].length ; j++){
                 board[i][j] = new Grid(sc.nextInt(), i, j);
            }
           
        }
        //set goal
        board[0][0].isi=1;
      
       
        //set kodok 
        int currentI = board.length-1 , currentJ = board.length-1;
        //set start
         board[currentI][currentJ].isi=2;
         Grid kodok = board[currentI][currentJ];
         
        
         while(kodok.i != 0 && kodok.j != 0 ){
          for(int i = board.length-1 ; i>=0 ; i--){
              double min = Float.MAX_VALUE;
               Grid nextRock = null;
               boolean foundRock = false;
            for(int j = board[0].length-1 ; j>=0 ; j--){
                if(board[i][j].isi == 1){
                     nextRock = board[i][j];
                  double dist = kodok.distance(nextRock);
                  if( kodok.canMove(dist)){
               //System.out.println(dist);
                    foundRock = true;
                    //eucildean heuristic is here 
                    double heuristik = board[i][j].heuristicEucil();
                    if(heuristik<min){
                        
                        min = heuristik;
                        nextRock.i = i;
                        nextRock.j = j;
                    }
                  }
                  
              }
          }
            
            if(foundRock){
                //check jarak
                 double dist =kodok.distance(nextRock);
                 //short jump
//                 System.out.println(dist);

            if(kodok.canMove(dist)){
                  if(kodok.isShortJump(dist)){
                time+=1.5;
                path.add(kodok);
            }
            //long jump
            else{
                time++;
                 path.add(kodok);
            }
                
            }
          
         
            //jump
            kodok = nextRock;
              
                
            }
            
           
      }
          path.add(board[0][0]);
             
         }
         System.out.printf("waktu yang di butuhkan : %.2f \n",time);
         System.out.println("Path yang di bentuk :");
         for(Grid pos : path){
             System.out.printf("(%d,%d) " , pos.i, pos.j);
         }
       
 
        
        
    }
    
  
    
}


 class Grid {
        int isi ,i , j;
    
    
    public Grid(int isi , int i , int j ){
        this.isi = isi;
        this.i = i;
        this.j = j;
       
    }
    
    public double heuristicEucil(){
        return Point2D.distance(this.i, this.j, 0, 0);
    }
     public double distance(Grid nextRock){
        return Point2D.distance(this.i, this.j, nextRock.i, nextRock.j);
    }
     public boolean canMove(double distance){
         return distance<=4.0;
         
     }
     
     public boolean isShortJump(double dist){
         return dist>2 && dist<=4;
     }
     
     
    
}
